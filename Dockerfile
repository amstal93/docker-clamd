# Dockerfile

FROM debian:10 As builder

ENV CLAMAV_VERSION 0.102.2

RUN apt-get -y update \
    && apt-get install -y \
        build-essential \
        openssl \
        libssl-dev \
        libcurl4-openssl-dev \
        zlib1g-dev \
        libpng-dev \
        libxml2-dev \
        libjson-c-dev \
        libbz2-dev \
        libpcre3-dev \
        ncurses-dev \
        libprelude-dev \
    && rm -rf /var/lib/apt/lists/*
    
ADD https://www.clamav.net/downloads/production/clamav-$CLAMAV_VERSION.tar.gz /tmp/clamav-$CLAMAV_VERSION.tar.gz

RUN cd /tmp \
    && tar xzf clamav-$CLAMAV_VERSION.tar.gz \
    && cd clamav-$CLAMAV_VERSION \
    && ./configure \
        --with-systemdsystemunitdir=no \
        --disable-clamav \
        --enable-prelude \
        --enable-clamdtop \
    && make all \
    && make install \
    && cd / \
    && rm -rf clamav-$CLAMAV_VERSION.tar.gz clamav-$CLAMAV_VERSION

FROM debian:10

ENV CLAMAV_VERSION 0.102.2

RUN apt-get -y update \
    && apt-get install -y \
        openssl \
        libssl1.1 \
        libcurl4 \
        zlib1g \
        libpng16-16 \
        libxml2 \
        libjson-c3 \
        libbz2-1.0 \
        libpcre3 \
        libncurses6 \
        supervisor \
        netcat \
        libprelude23 \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/sbin/clamd /usr/local/sbin/clamd
COPY --from=builder /usr/local/bin/clamdtop /usr/local/bin/clamdtop
COPY --from=builder /usr/local/bin/freshclam /usr/local/bin/freshclam
COPY --from=builder /usr/local/lib /usr/local/lib

RUN ldconfig

COPY config/clamav/clamd.conf /usr/local/etc/clamd.conf
COPY config/clamav/freshclam.conf /usr/local/etc/freshclam.conf

RUN useradd -r -m -U -d /var/lib/clamav clamav \
    && mkdir -p /run/clamav /var/log/clamav \
    && chown -R clamav:clamav /var/lib/clamav /run/clamav /var/log

# Populate the database
RUN /usr/local/bin/freshclam

COPY config/supervisor/supervisord.conf /etc/supervisord.conf
COPY config/supervisor/clamd.ini /etc/supervisor.d/clamd.ini
COPY config/supervisor/freshclam.ini /etc/supervisor.d/freshclam.ini

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

COPY healthcheck.sh /usr/local/bin/healthcheck.sh
RUN chmod +x /usr/local/bin/healthcheck.sh
HEALTHCHECK --interval=60s --timeout=5s --start-period=15s --retries=3 CMD [ "/usr/local/bin/healthcheck.sh" ]

WORKDIR /var/lib/clamav
USER clamav

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]

VOLUME ["/var/lib/clamav"]
EXPOSE 3310

LABEL org.opencontainers.image.title "ClamAV daemon"
ARG IMAGE_VERSION=snapshot
LABEL org.opencontainers.image.version "${CI_COMMIT_REF_NAME}"
LABEL org.opencontainers.image.description "Recent version of ClamAV anti-virus daemon"
LABEL org.opencontainers.image.authors "Gregory Romé"
LABEL org.opencontainers.image.url "https://gitlab.com/gpr-oss/docker-clamd"
LABEL org.opencontainers.image.source "https://gitlab.com/gpr-oss/docker-clamd"
